const Tutorial = require("../models/tutorial.model.js");

exports.create = async (req, res) => {
  if (!req.body) {
    res.status(400).json({
      message: "Content can not be empty!",
    });
  }

  const tutorial = new Tutorial({
    title: req.body.title,
    description: req.body.description,
    published: req.body.published || false,
  });

  try {
    const data = await Tutorial.create(tutorial);
    res.json(data);
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

exports.findAll = async (req, res) => {
  const title = req.query.title;
  try {
    const data = await Tutorial.getAll(title);
    res.json(data);
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

exports.findAllPublished = async (req, res) => {
  try {
    const data = await Tutorial.getAllPublished();
    res.json(data);
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

exports.findOne = async (req, res) => {
  try {
    const data = await Tutorial.findById(req.params.id);
    if (data.kind === "not_found") {
      res.status(404).json({
        message: `Not found Tutorial with id ${req.params.id}`,
      });
    } else {
      res.json(data);
    }
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

exports.update = async (req, res) => {
  try {
    if (!req.body) {
      res.status(400).json({
        message: "Content can not be empty",
      });
    }

    const data = await Tutorial.updateById(
      req.params.id,
      new Tutorial(req.body)
    );
    if (data.kind === "not_found") {
      res.status(404).json({
        message: `Not found Tutorial with id ${req.params.id}`,
      });
    } else {
      res.json(data);
    }
  } catch (err) {
    res.status(500).json({
      message: "Error updating Tutorial with id " + req.params.id,
    });
  }
};

exports.delete = async (req, res) => {
  try {
    const data = await Tutorial.remove(req.params.id);

    if (data.kind === "not_found") {
      res.status(404).json({
        message: `Not found Tutorial with id ${req.params.id}`,
      });
    }

    res.json(data);
  } catch (err) {
    res.status(500).json({
      message: "Could not delete Tutorial with id " + req.params.id,
    });
  }
};

exports.deleteAll = async (req, res) => {
  try {
    await Tutorial.removeAll();
    res.json({ message: "All Tutorials were deleted successfully" });
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

// const create = require("../controllers/tutorial.controller.js");

const express = require("express");

const tutorials = require("../controllers/tutorial.controller.js");

let router = express.Router();

//   app.use("/api/tutorials", router);

router.post("/", tutorials.create);

router.get("/", tutorials.findAll);

router.get("/published", tutorials.findAllPublished);

router.get("/:id", tutorials.findOne);

router.put("/:id", tutorials.update);

router.delete("/:id", tutorials.delete);

router.delete("/", tutorials.deleteAll);

module.exports = router;

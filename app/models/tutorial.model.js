const pool = require("./db.js");

const Tutorial = function (tutorial) {
  this.title = tutorial.title;
  this.description = tutorial.description;
  this.published = tutorial.published;
};

Tutorial.create = async (newTutorial) => {
  const values = [
    newTutorial.title,
    newTutorial.description,
    newTutorial.published,
  ];
  try {
    const res = await pool.query(
      "INSERT INTO tutorials  (title,description,published) values ($1,$2,$3) RETURNING *;",
      values
    );
    return res.rows;
  } catch (error) {
    throw error;
  }
};

Tutorial.getAll = async (title) => {
  let query = "SELECT * FROM tutorials";

  if (title) {
    query += ` WHERE title LIKE '%${title}%'`;
  }

  try {
    const res = await pool.query(query);
    return res.rows;
  } catch (error) {
    throw error;
  }
};

Tutorial.getAllPublished = async () => {
  try {
    const res = await pool.query(
      "SELECT * FROM tutorials WHERE published = true"
    );
    return res.rows;
  } catch (error) {
    throw error;
  }
};

Tutorial.findById = async (id) => {
  try {
    const res = await pool.query(`SELECT * FROM tutorials WHERE id=${id}`);

    if (res.rows.length) {
      return res.rows;
    }
    return { kind: "not_found" };
  } catch (error) {
    throw error;
  }
};

Tutorial.updateById = async (id, tutorial) => {
  try {
    const res = await pool.query(
      "UPDATE tutorials SET title = $1, description = $2, published = $3 WHERE id = $4",
      [tutorial.title, tutorial.description, tutorial.published, id]
    );
    if (res.rowCount == 0) {
      return { kind: "not_found" };
    }
    return "updated Successfully";
  } catch (error) {
    throw error;
  }
};

Tutorial.remove = async (id) => {
  try {
    const res = await pool.query("DELETE FROM tutorials WHERE id = $1", [
      id,
    ]);
    if (res.rowCount == 0) {
      return { kind: "not_found" };
    }
    return `${id} deleted Successfully`;
  } catch (error) {
    throw error;
  }
};

Tutorial.removeAll = async () => {
  try {
    const res = await pool.query("DELETE FROM tutorials");
    return "deleted all rows successfully";
  } catch (error) {
    throw error;
  }
};

module.exports = Tutorial;


const {Pool} = require("pg")
const dbconfig = require('../config/db.config.js')

const pool = new Pool({
    host : dbconfig.HOST,
    user : dbconfig.USER,
    password : dbconfig.PASSWORD,
    database : dbconfig.DB
})

module.exports = pool
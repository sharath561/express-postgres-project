const express = require("express");
const pool = require("./app/models/db");
const app = express();

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

const tutorialRoutes = require("./app/routes/tutorial.routes.js");
app.use("/api/tutorials", tutorialRoutes);

app.get("/", (req, res) => {
  res.json({ message: "Welcome to tutorials database" });
});

const PORT = 5050;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
